<?php

namespace App\Http\Controllers;

use App\Models\ImportUsers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{

    public function index(Request $request)
    {


        if ($request->filled('import')) {
            ImportUsers::importUser();
        }


        $inserted = explode(',', Storage::get('statistic.txt'));

        if (!isset($inserted[1])) {
            $inserted[0] = 0;
            $inserted[1] = 0;
        }

        $users = ImportUsers::get();
        return view('welcome', ['users' => count($users),
            'inserted' => $inserted,
            ] );
    }


    public function api()
    {
        $users = ImportUsers::get();

        $inserted = explode(',', Storage::get('statistic.txt'));

        if (!isset($inserted[1])) {
            $inserted[0] = 0;
            $inserted[1] = 0;
        }

        return json_encode(['countUsers' => count($users),'inserted' => $inserted[1], 'updated' => $inserted[0]] );
    }
}
