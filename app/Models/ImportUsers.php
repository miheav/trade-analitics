<?php

namespace App\Models;

use App\Helpers\CurlHelper;
use Illuminate\Database\Eloquent\Model;

class ImportUsers extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'age',
    ];


    static function importUser()
    {

        dispatch(new \App\Jobs\ImportUsers("https://randomuser.me/api/?results=5000"));

    }
}
