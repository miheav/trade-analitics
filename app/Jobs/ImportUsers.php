<?php

namespace App\Jobs;

use App\Helpers\CurlHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ImportUsers as User;
use Illuminate\Support\Facades\Storage;
class ImportUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $results;
    /**
     * Create a new job instance.
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        $data = CurlHelper::getPage($this->url);

        $this->results = json_decode($data, true)['results'];
        $result[0] = 0;
        $result[1] = 0;
        $counter = 0;
        Storage::put('statistic.txt', "0,0");

        foreach ($this->results as $row) {

            $user = User::where('last_name', $row['name']['last'])->where('first_name', $row['name']['first'])->first();
            if ($user) {
                $user->first_name = $row['name']['first'];
                $user->last_name = $row['name']['last'];
                $user->email = $row['email'];
                $user->age = $row['dob']['age'];
                $user->save();
                $result[0]++;
            } else {
                $user = new User();
                $user->first_name = $row['name']['first'];
                $user->last_name = $row['name']['last'];
                $user->email = $row['email'];
                $user->age = $row['dob']['age'];
                $user->save();
                $result[1]++;
            }

            if ($counter % 20 == 0) {
                Storage::put('statistic.txt', "$result[0],$result[1]");
            }
            $counter++;
        }
        Storage::put('statistic.txt', "$result[0],$result[1]");
    }
}
